Development stack
==================

The purpose of this repo is to speed up the process to setup new projects
for front-end development.

Contains:
 * Build tolls: browserify
 * Testing tools: Mocha and Chai
 * Karma setup for Chrome, Firefox and Safari
 * SauceLabs setup for continous integration


Pre-requisites
-------------

* NodeJS needs to be installed. Preferably with
  [nvm](https://github.com/creationix/nvm). I typically use the latest stable
  node version. See with `nvm ls-remote`

* Bower for managing browser modules: `npm install -g bower`


Getting started
--------------

 * Installation: `npm install; npm run-script init`
 * [browserify](http://browserify.org) is used to manage modules. Build the
   JavaScript with `npm run-script compile`. Make sure that browserify is
   installed first: `npm install -g browserify`
 * Run tests: `npm run-script test`
 * Setup the configuration in `setenv` for your SauceLabs account using `setenv.template`.
   Run SauceLab tests: `npm run-script sauce`
 * Check that your code follows th code style: `npm run-script style`
