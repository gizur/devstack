// template.js
//------------------------------
//
// 2015-04-01, Jonas Colmsjö
//------------------------------
//
// Template
//
// Usage:
//   Compile with browserify or include like this:
//   <script src='./src/tesmplate.js'></script>
//
//
// Using Google JavaScript Style Guide:
// http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------

var Template = {};

Template.MODULE_NAME = 'Template';

// Put your code here

Template = function(options) {

};

Template.prototype.func = function() {

};

//
// Export module
// ---------------------------------------------------------------------------
// This is a small hack to make it possbile to use the module both with and
// without browserify
//
// traditional include:   <script src="./src/varp/term.js"></script>
// use `require('template.js')` in `main.js` when building with browserify:
// `browserify -d main.js -o bundle.js`

if (typeof inBrowser === 'undefined') {
  inBrowser = (typeof module === 'undefined');
}

if (inBrowser) {
  window.module = {
    exports: {}
  };
}

// The name of the module becomes the filename automatically in browserify
module.exports = Template;

// export the module without browserify
if (inBrowser) {
  window[Template.MODULE_NAME] = module.exports;
}
