module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    browserify: {
      build: {
        src: './test/main.js',
        dest: './build/test/main.js'
      }
    },
    watch: {
      files: ['**/*'],
      tasks: ['default'],
    }

  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browserify');

  // Default task(s).
  grunt.registerTask('default', [
  'browserify'
  ]);

};
