module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! Grunt Uglify <%= grunt.template.today("yyyy-mm-dd") %> */',
        sourceMap: true
      },
      build: {
        src: './build/main.js',
        dest: './build/main.min.js'
      }
    },
    browserify: {
      build: {
        src: './src/main.js',
        dest: './build/main.js'
      }
    },
    watch: {
      files: ['**/*'],
      tasks: ['default'],
    }

  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browserify');

  // Default task(s).
  grunt.registerTask('default', [
  'browserify',
  'uglify'
  ]);

};
