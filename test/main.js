// main.js
//------------------------------
//
// 2015-03-31, Jonas Colmsjö
//
//------------------------------
//
// The purpose of this file is to make it possible to build with `browserify`.
// The contents must be equivalent with index-nobuild.html
//
// Using Google JavaScript Style Guide:
// http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------

var Helpers = require('../src/template.js');

// The tests
var TestPage = require('./tests.js');
