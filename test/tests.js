var chai = require('chai');
var assert = chai.assert;

describe('My assert tests...', function() {
  'use strict';

  var ourQuiz;
  var contestData = {
    question1: 'How Are you?',
    answer1: 'Fine, thanks.',
    question2: 'Status',
    answer2: 'Great',
    question3: 'Who is the greatest painter?',
    answer3: 'Raphael'
  };

  before(function() {
    console.log('In the before function...');
  });

  after(function() {
    console.log('In the after function...');
  });

  it('should correctly list data', function(done) {
    var foo = 'bar';
    var beverages = {
        tea: ['chai', 'matcha', 'oolong']
      };

    assert.typeOf(foo, 'string', 'foo is a string');
    assert.equal(foo, 'bar', 'foo equal `bar`');
    assert.lengthOf(foo, 3, 'foo`s value has a length of 3');
    assert.lengthOf(beverages.tea, 3, 'beverages has 3 types of tea');

    // This will break
    assert.lengthOf(beverages.tea, 3, 'beverages has 3 types of tea');

    // necessary for async tests
    done();

  });

});
